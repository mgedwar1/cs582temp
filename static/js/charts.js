queue()
    .defer(d3.json, "/cs582")
    .await(makeGraphs);

function makeGraphs(error, json_posts){
    var posts = json_posts;
    var dateFormat = d3.time.format("%Y-%m-%d");
    posts.forEach(function(d) {
        d["status_published"] = dateFormat.parse(d["status_published"]);
        d["date_posted"].setDate(1);
    });
    var ndx = crossfilter(posts);
    var num_loves = ndx.dimension(function(d){ return d["num_loves"]});
    var date = ndx.dimension(function(d){return d["status_published"]})
    var all = ndx.groupAll();

    var lovesToDate = dc.barChart("#time-chart");
    timeChart
    .width(600)
    .height(160)
    .margins({top: 10, right: 50, bottom: 30, left: 50})
    .dimension(dateDim)
    .group(numProjectsByDate)
    .transitionDuration(500)
    .x(d3.time.scale().domain([minDate, maxDate]))
    .elasticY(true)
    .xAxisLabel("Year")
    .yAxis().ticks(4);

    dc.renderAll();
}