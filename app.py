from flask import Flask
from flask import render_template
from pymongo import MongoClient
import json
from bson import json_util
from bson.json_util import dumps
from flask import request
import datetime
import isodate

app = Flask(__name__)

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'postsWSent'
COLLECTION_NAME = 'postsWSent'
FIELDS = {'_id':False, 'status_link':True,'num_loves':True,'news_site':True,'num_angrys':True,'num_likes':True,'news_key_words':True,'num_comments':True,
    'num_reactions':True,'negative':True,'link_name':True,'status_published':True,'media_type':True,'status_message':True,'status_type':True,'status_id':True,
    'num_hahas':True,'source_type':True,'neutral':True,'news_content':True,'compount':True,'permalink_url':True,'num_sads':True,'num_wows':True,
    'positive':True,'num_shares':True}


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/cs582/",methods=["GET"])
def postsWSent():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION_NAME]

    date_handler = lambda obj: (
        obj.isoformat()
        if isinstance(obj, datetime.datetime)
           or isinstance(obj, datetime.date)
        else None
    )

    base = json.dumps(request.args.get('start',None), default=date_handler)
    final = json.dumps(request.args.get('end', None), default=date_handler)
    posts = collection.find({'status_published':{'$gte':isodate.parse_date(base),'$lte':isodate.parse_date(final)}},limit=int(request.args.get('numPosts',None)))
    json_posts = []
    s = ""
    for post in posts:
        json_posts.append(post)

    json_posts = json.dumps(json_posts, default=json_util.default)
    connection.close()
    print(s)
    for post in json_posts:
        s+=post
    print(s)
    return json_posts

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)